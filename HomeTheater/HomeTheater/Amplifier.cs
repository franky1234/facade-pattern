﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeTheater
{
    public class Amplifier //for increase the volume
    {
        public Tuner tuner { get; set; }
        public DvdPlayer dvdPlayer { get; set; }
        public CdPlayer cdPlayer { get; set; }
        public Cd cd { get; set; }
        public Dvd dvd { get; set; }
        public Amplifier()
        {
            tuner = new Tuner();
            dvdPlayer = new DvdPlayer();
            cdPlayer = new CdPlayer();
        }
        public void on() {
            Console.WriteLine($"Turn on the amplifier.");
        }
        public void off()
        {
            Console.WriteLine($"Turn off the amplifier.");
        }
        public void setCd(CdPlayer newCd)
        {
            cdPlayer = newCd;
            Console.WriteLine($"Setting the CD.");
        }
        public void setDvd(DvdPlayer newDvd)
        {
            dvdPlayer = newDvd;
            Console.WriteLine($"Setting the Dvd Player.");
        }
        public void setStereoSound() {
            Console.WriteLine($"Setting the Stereo sound.");
        }
        public void setSurroundSound()
        {
            Console.WriteLine($"Setting the Surround sound.");
        }
        public void setTurner()
        {
            Console.WriteLine($"Setting the tuner.");
            tuner.setFrecuency("FM");
        }
        public void setVolume(int volume)
        {
            Console.WriteLine($"Setting the Volume.");
        }
    }
}
