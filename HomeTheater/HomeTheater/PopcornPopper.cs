﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeTheater
{
    public class PopcornPopper
    {
        public int quantity { get; set; }
        public PopcornPopper()
        {
            quantity = 0;
        }
        public void on() {
            Console.WriteLine($"Turn on the popcorn popper.");
        }
        public void off() {
            Console.WriteLine($"Turn off the popcorn popper.");
        }
        public void pop() {
            Console.WriteLine($"Preparing popcorns.");
        }
    }
}
