﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeTheater
{
    public class DvdPlayer
    {
        public Amplifier amplifier { get; set; }
        public DvdPlayer()
        {
            //amplifier = new Amplifier();
        }
        public void on() {
            Console.WriteLine($"Turn on the DvdPlayer.");
        }
        public void off()
        {
            Console.WriteLine($"Turn off the DvdPlayer.");
        }
        public void eject()
        {
            Console.WriteLine($"Eject the Dvd.");
        }
        public void pause()
        {
            Console.WriteLine($"Pause the DvdPlayer.");
        }
        public void play(String movie)
        {
            Console.WriteLine($"Pause the DvdPlayer.");
        }
        public void setSurroundAudio()
        {
            Console.WriteLine($"Setting the surround audio.");
        }
        public void setTwoChannelAudio()
        {
            Console.WriteLine($"Setting the two channel audio.");
        }
        public void stop()
        {
            Console.WriteLine($"Stop the surround audio.");
        }
    }
}
