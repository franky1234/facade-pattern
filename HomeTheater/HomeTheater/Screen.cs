﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeTheater
{
    public class Screen
    {
        public int pixel { get; set; }
        public Screen()
        {
            pixel = 0;
        }
        public void up() {
            Console.WriteLine($"Upping the screen.");
        }
        public void down() {
            Console.WriteLine($"Downing the screen.");
        }
    }
}
