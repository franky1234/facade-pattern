﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeTheater
{
    class Program
    {
        static void Main(string[] args)
        {
            Amplifier amp;
            Tuner tuner;
            DvdPlayer dvd;
            CdPlayer cd;
            Projector projector;
            TheaterLights lights;
            Screen screen;
            PopcornPopper popper;

            amp = new Amplifier();
            tuner = new Tuner();
            dvd = new DvdPlayer();
            cd = new CdPlayer();
            projector = new Projector();
            lights = new TheaterLights();
            screen = new Screen();
            popper = new PopcornPopper();

            /*popper.on();
            popper.pop();
            lights.dim(10);
            screen.down();
            projector.on();
            projector.wideScreenMode();
            amp.on();
            amp.setDvd(dvd);
            amp.setSurroundSound();
            amp.setVolume(5);
            dvd.on();
            dvd.play("Godzilla");*/

            HomeTheaterFacade homeFacade = new HomeTheaterFacade(
                amp,
                tuner,
                dvd,
                cd,
                projector,
                screen,
                lights,
                popper
            );
            homeFacade.watchMovie("Star Wars I");
            homeFacade.endMovie();
            Console.ReadKey(true);
        }
    }
}
