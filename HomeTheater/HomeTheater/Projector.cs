﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeTheater
{
    public class Projector
    {
        DvdPlayer dvdPlayer { get; set; }
        public Projector()
        {
            dvdPlayer = new DvdPlayer();
        }
        public void on()
        {
            Console.WriteLine($"Turn on the Projector.");
        }
        public void off() {
            Console.WriteLine($"Turn off the Projector.");
        }
        public void tvMode()
        {
            Console.WriteLine($"Set to TV mode.");
        }
        public void wideScreenMode()
        {
            Console.WriteLine($"Setting the wide screen.");
        }
    }
}
