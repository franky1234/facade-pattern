﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeTheater
{
    public class CdPlayer
    {
        public Amplifier amplifier { get; set; }
        public CdPlayer()
        {
            //amplifier = new Amplifier();
        }
        public void on() {
            Console.WriteLine($"Turn on the CdPlayer.");
        }
        public void off()
        {
            Console.WriteLine($"Turn off the CdPlayer.");
        }
        public void eject()
        {
            Console.WriteLine($"Eject the Cd.");
        }
        public void pause()
        {
            Console.WriteLine($"Pause the CdPlayer.");
        }
        public void play()
        {
            Console.WriteLine($"Plays the CdPlayer.");
        }
        public void stop()
        {
            Console.WriteLine($"Stop the CdPlayer.");
        }
    }
}
