﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeTheater
{
    public class Tuner
    {
        public Amplifier amplifier { get; set; }
        public Tuner()
        {
            //amplifier = new Amplifier();
        }
        public void on() {
            Console.WriteLine($"Turner is on");
        }
        public void off()
        {
            Console.WriteLine($"Turner is off");
        }
        public void setAm(String time)
        {
            Console.WriteLine($"Setting to: { time } AM");
        }
        public void setFm(String fm)
        {
            Console.WriteLine($"Setting fm to: { fm }");
        }
        public void setFrecuency(String frecuency)
        {
            Console.WriteLine($"Setting frecuency to: { frecuency }");
        }
    }

}
